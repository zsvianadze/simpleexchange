package com.zs;

import com.zs.dao.Dao;
import com.zs.dao.DaoFactory;
import com.zs.entity.*;
import com.zs.ref.DealDirection;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrderMatcherTest {

    @Test
    public void testOrderMatcher() {
        Dao dao = DaoFactory.getInstance();
        Security a = dao.getSecurityByCode("A");
        OrderQueue queue = new OrderQueue(a);
        OrderMatcher matcher = new OrderMatcher(queue);

        Client client1 = new Client("C1",
                new ClientBalance(
                        new MoneyAccount(BigDecimal.TEN),
                        Collections.singletonList(
                                new SecurityAccount(a, BigDecimal.TEN)
                        )));

        Client client2 = new Client("C2",
                new ClientBalance(
                        new MoneyAccount(BigDecimal.TEN),
                        new ArrayList<>()));

        Client client3 = new Client("C3",
                new ClientBalance(
                        new MoneyAccount(BigDecimal.TEN),
                        new ArrayList<>()));

        queue.add(new Order(client1, a, DealDirection.SELL, new BigDecimal(6), BigDecimal.valueOf(0.6)));
        queue.add(new Order(client2, a, DealDirection.BUY, new BigDecimal(5), BigDecimal.valueOf(0.5)));//Слишком низкая цена
        queue.add(new Order(client3, a, DealDirection.BUY, new BigDecimal(5), BigDecimal.valueOf(0.7)));

        Assert.assertFalse(queue.isSellEmpty());//rest order from client1
        Assert.assertFalse(queue.isBuyEmpty());//order from client2

        List<Deal> deals = dao.getAllDeals();
        Assert.assertFalse(deals.isEmpty());
        Deal deal = deals.get(deals.size() - 1);
        Assert.assertEquals(a, deal.getBuyOrder().getSecurity());
        Assert.assertEquals(0, BigDecimal.valueOf(0.7).compareTo(deal.getPrice()));
        Assert.assertEquals(0, BigDecimal.valueOf(5).compareTo(deal.getQuantity()));
    }
}
