package com.zs.dao;

import com.zs.entity.*;
import com.zs.ref.DealDirection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class MemDaoTest {

    private Dao dao;
    private static final String BUY_CLIENT_NAME = "B1", SELL_CLIENT_NAME= "S1";

    @Before
    public void init() {
        dao = new MemDao();
        //For deals
        dao.insertClient(createTestClient(BUY_CLIENT_NAME));
        dao.insertClient(createTestClient(SELL_CLIENT_NAME));
    }

    @Test
    public void testGetSecurityByCode() {
        Security securityA = dao.getSecurityByCode("A");
        Assert.assertNotNull(securityA);
        Assert.assertEquals("A", securityA.getCode());

        Security securityX = dao.getSecurityByCode("X");
        Assert.assertNull(securityX);
    }

    @Test
    public void testInsertClient() {
        Client client = createTestClient("C1");
        dao.insertClient(client);
        Client insertedClient = dao.getClientByName("C1");
        Assert.assertEquals(client, insertedClient);
    }

    @Test
    public void testGetAllClients() {
        Set<Client> clients = dao.getAllClients();
        Client client = createTestClient("GAC1");
        dao.insertClient(client);
        Set<Client> clientsAfterInsert = dao.getAllClients();
        Assert.assertTrue(!clientsAfterInsert.isEmpty());
        Assert.assertEquals(clients.size() + 1, clientsAfterInsert.size());
    }

    @Test
    public void testGetClientByName() {
        Client client = dao.getClientByName(BUY_CLIENT_NAME);
        Assert.assertNotNull(client);
        Assert.assertEquals(BUY_CLIENT_NAME, client.getName());
    }

    @Test
    public void testInsertDeal() {
        Deal deal = createTestDeal();
        dao.insertDeal(deal);
        List<Deal> dealsAfterInsert = dao.getAllDeals();
        Assert.assertTrue(dealsAfterInsert.size() > 0);
        Assert.assertEquals(deal, (dealsAfterInsert).get(dealsAfterInsert.size() - 1));
    }

    @Test
    public void testGetAllDeals() {
        List<Deal> dealsBeforeInsert = dao.getAllDeals();
        Deal deal = createTestDeal();
        dao.insertDeal(deal);
        List<Deal> dealsAfterInsert = dao.getAllDeals();
        Assert.assertTrue(!dealsAfterInsert.isEmpty());
        Assert.assertEquals(dealsBeforeInsert.size() + 1, dealsAfterInsert.size());
    }

    private Client createTestClient(String name) {
        return new Client(name,
                new ClientBalance(
                        new MoneyAccount(BigDecimal.TEN),
                        Collections.singletonList(new SecurityAccount(dao.getSecurityByCode("A"), BigDecimal.ONE))));
    }

    private Deal createTestDeal() {
        Client buyClient = dao.getClientByName(BUY_CLIENT_NAME);
        Client sellClient = dao.getClientByName(SELL_CLIENT_NAME);
        Security security = dao.getSecurityByCode("A");
        Order buyOrder = new Order(buyClient, security, DealDirection.BUY, BigDecimal.valueOf(1), BigDecimal.valueOf(5));
        Order sellOrder = new Order(sellClient, security, DealDirection.SELL, BigDecimal.valueOf(1), BigDecimal.valueOf(5));
        return new Deal(buyOrder, sellOrder, BigDecimal.ONE, BigDecimal.valueOf(5));
    }
}
