package com.zs;

import com.zs.entity.*;
import com.zs.ref.DealDirection;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collections;

public class ClientBalanceUpdaterTest {

    @Test
    public void testUpdateWithDeal() {
        Security a = new Security("A");

        Client client1 = new Client("C1",
                new ClientBalance(
                        new MoneyAccount(BigDecimal.TEN),
                        Collections.singletonList(
                                new SecurityAccount(a, BigDecimal.ONE)
                        )));

        Client client2 = new Client("C2",
                new ClientBalance(
                        new MoneyAccount(BigDecimal.TEN),
                        Collections.singletonList(
                                new SecurityAccount(a, BigDecimal.ONE)
                        )));

        Order buyOrder = new Order(client1, a, DealDirection.BUY, BigDecimal.ONE, BigDecimal.TEN);
        Order sellOrder = new Order(client2, a, DealDirection.SELL, BigDecimal.ONE, BigDecimal.TEN);

        Deal deal = new Deal(buyOrder, sellOrder, BigDecimal.ONE, BigDecimal.TEN);

        ClientBalanceUpdater.updateWith(deal);

        ClientBalance clientBalance1 = client1.getClientBalance();
        ClientBalance clientBalance2 = client2.getClientBalance();

        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(clientBalance1.getMoneyAccount().getAmount()));
        Assert.assertEquals(0, BigDecimal.valueOf(20).compareTo(clientBalance2.getMoneyAccount().getAmount()));

        Assert.assertEquals(0, BigDecimal.valueOf(2).compareTo(clientBalance1.getSecurityAccountFor(a).getQuantity()));
        Assert.assertEquals(0, BigDecimal.ZERO.compareTo(clientBalance2.getSecurityAccountFor(a).getQuantity()));
    }
}
