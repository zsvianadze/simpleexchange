package com.zs;

import java.io.IOException;

public class SimpleExchange {

    public static void main(String[] args) throws IOException {
        ClientReader.read("clients.txt");
        OrderProducer.produce("orders.txt");
        ClientWriter.write("result.txt");
        DealWriter.write("deals.txt");
    }
}
