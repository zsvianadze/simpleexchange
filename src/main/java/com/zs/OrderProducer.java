package com.zs;

import com.zs.dao.DaoFactory;
import com.zs.entity.Client;
import com.zs.entity.Order;
import com.zs.entity.Security;
import com.zs.ref.DealDirection;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Класс для считывания заявок из файла и помещения их в соответствующие очереди заявок (в разрезе ценной бумаги)
 */
public class OrderProducer {

    public static void produce(String fileName) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.map(OrderProducer::parseOrder).
                    forEach(o -> OrderQueueMatcherHolder.getOrderQueueFor(o.getSecurity()).add(o));
        }
    }

    private static Order parseOrder(String orderStr) {
        String[] fields = orderStr.split("\t");
        Client client = DaoFactory.getInstance().getClientByName(fields[0]);
        DealDirection dealDirection = fields[1].equals("b") ? DealDirection.BUY : DealDirection.SELL;
        Security security = DaoFactory.getInstance().getSecurityByCode(fields[2]);
        BigDecimal price = new BigDecimal(fields[3]);
        BigDecimal quantity = new BigDecimal(fields[4]);
        return new Order(client, security, dealDirection, quantity, price);
    }
}
