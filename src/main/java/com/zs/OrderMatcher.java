package com.zs;

import com.zs.dao.DaoFactory;
import com.zs.entity.Deal;
import com.zs.entity.Order;

import java.math.BigDecimal;
import java.util.Observable;
import java.util.Observer;

/**
 * Класс обработки очереди заявок на покупку и продажу
 */
public class OrderMatcher implements Observer {

    private OrderQueue orderQueue;

    public OrderMatcher(OrderQueue orderQueue) {
        this.orderQueue = orderQueue;
        orderQueue.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        processQueue();
    }

    /**
     * Метод обрабатывает очередь заявок.
     * Если сделка из заявок на покупку и продажу возможна,
     * метод формирует сделку, сохраняет ее в хранилище сделок,
     * возвращает частично исполненные заявки обратно в очередь,
     * инициирует изменения баланса клиентов.
     */
    private void processQueue() {
        if (isDealPossible()) {
            Order buyOrder = orderQueue.pollBuy();
            Order sellOrder = orderQueue.pollSell();
            BigDecimal dealPrice = buyOrder.getPrice();
            BigDecimal dealQuantity = buyOrder.getQuantity().min(sellOrder.getQuantity());
            Deal deal = new Deal(buyOrder, sellOrder, dealQuantity, dealPrice);
            DaoFactory.getInstance().insertDeal(deal);
            checkOrderToReturn(buyOrder, dealQuantity);
            checkOrderToReturn(sellOrder, dealQuantity);
            ClientBalanceUpdater.updateWith(deal);
        }
    }

    private void checkOrderToReturn(Order order, BigDecimal dealQuantity) {
        if (order.getQuantity().compareTo(dealQuantity) > 0) {
            order.setQuantity(order.getQuantity().subtract(dealQuantity));
            orderQueue.add(order);
        }
    }

    /**
     * Метод проверяет, возможно ли лучшие заявки из очередей на покупку и продажу объединить в сделку.
     * @return
     * true - сделка возможна, false - сделка невозможна
     */
    private boolean isDealPossible() {
        if (!orderQueue.isBuyEmpty() && !orderQueue.isSellEmpty()) {
            BigDecimal buyPrice = orderQueue.peekBuy().getPrice();
            BigDecimal sellPrice = orderQueue.peekSell().getPrice();
            return buyPrice.compareTo(sellPrice) >= 0;
        }
        return false;
    }
}
