package com.zs.entity;

import java.math.BigDecimal;

public class MoneyAccount {

    private BigDecimal amount;

    public MoneyAccount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
