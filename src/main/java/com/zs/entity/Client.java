package com.zs.entity;

public class Client {
    private String name;
    private ClientBalance clientBalance;

    public Client(String name, ClientBalance clientBalance) {
        this.name = name;
        this.clientBalance = clientBalance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClientBalance getClientBalance() {
        return clientBalance;
    }

    public void setClientBalance(ClientBalance clientBalance) {
        this.clientBalance = clientBalance;
    }
}
