package com.zs.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClientBalance {
    private final MoneyAccount moneyAccount;
    private final List<SecurityAccount> securityAccounts;
    private final Map<Security, SecurityAccount> securityAccountIndex;

    public ClientBalance(MoneyAccount moneyAccount, List<SecurityAccount> securityAccounts) {
        this.moneyAccount = moneyAccount;
        this.securityAccounts = securityAccounts;
        securityAccountIndex = new HashMap<>();
        securityAccounts.forEach(s -> securityAccountIndex.put(s.getSecurity(), s));
    }

    public MoneyAccount getMoneyAccount() {
        return moneyAccount;
    }

    public List<SecurityAccount> getSecurityAccounts() {
        return securityAccounts;
    }

    public void addSecurityAccount(SecurityAccount securityAccount) {
        securityAccounts.add(securityAccount);
        securityAccountIndex.put(securityAccount.getSecurity(), securityAccount);
    }

    public SecurityAccount getSecurityAccountFor(Security security) {
        return securityAccountIndex.get(security);
    }
}
