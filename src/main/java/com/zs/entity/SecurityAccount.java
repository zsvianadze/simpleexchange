package com.zs.entity;

import java.math.BigDecimal;

public class SecurityAccount {

    private final Security security;
    private BigDecimal quantity;

    public SecurityAccount(Security security, BigDecimal quantity) {
        this.security = security;
        this.quantity = quantity;
    }

    public Security getSecurity() {
        return security;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
