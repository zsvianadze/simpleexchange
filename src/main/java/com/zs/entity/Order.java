package com.zs.entity;

import com.zs.ref.DealDirection;

import java.math.BigDecimal;

public class Order {

    private Client client;
    private Security security;
    private DealDirection direction;
    private BigDecimal quantity;
    private BigDecimal price;

    public Order(Client client, Security security, DealDirection direction, BigDecimal quantity, BigDecimal price) {
        this.client = client;
        this.security = security;
        this.direction = direction;
        this.quantity = quantity;
        this.price = price;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public DealDirection getDirection() {
        return direction;
    }

    public void setDirection(DealDirection direction) {
        this.direction = direction;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
