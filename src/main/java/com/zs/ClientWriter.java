package com.zs;

import com.zs.dao.DaoFactory;
import com.zs.entity.Client;
import com.zs.entity.ClientBalance;
import com.zs.entity.Security;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Класс для записи финальных балансов клиентов в файл
 */
public class ClientWriter {

    public static void write(String fileName) throws IOException {
        try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)))) {
            DaoFactory.getInstance().getAllClients().stream().map(ClientWriter::formatClient).forEach(pw::println);
        }
    }

    private static String formatClient(Client client) {
        StringBuilder clientStrBuilder = new StringBuilder();
        clientStrBuilder.append(client.getName()).append("\t");
        ClientBalance cb = client.getClientBalance();
        clientStrBuilder.append(cb.getMoneyAccount().getAmount()).append("\t");
        for (String securityCode : Arrays.asList("A", "B", "C", "D")) {
            Security security = DaoFactory.getInstance().getSecurityByCode(securityCode);
            clientStrBuilder.append(cb.getSecurityAccountFor(security).getQuantity()).append("\t");
        }
        return clientStrBuilder.toString();
    }
}
