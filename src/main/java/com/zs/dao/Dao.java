package com.zs.dao;

import com.zs.entity.Client;
import com.zs.entity.Deal;
import com.zs.entity.Security;

import java.util.List;
import java.util.Set;

public interface Dao {

    Security getSecurityByCode(String code);

    void insertClient(Client client);

    Client getClientByName(String name);

    Set<Client> getAllClients();

    void insertDeal(Deal deal);

    List<Deal> getAllDeals();
}
