package com.zs.dao;

import com.zs.entity.Client;
import com.zs.entity.Deal;
import com.zs.entity.Security;

import java.util.*;

public class MemDao implements Dao {

    private Map<String, Security> securityIndex = new HashMap<String, Security>();
    {
        securityIndex.put("A", new Security("A"));
        securityIndex.put("B", new Security("B"));
        securityIndex.put("C", new Security("C"));
        securityIndex.put("D", new Security("D"));
    }

    Set<Client> clientList = new LinkedHashSet<>();
    Map<String, Client> clientIndex = new HashMap<>();

    List<Deal> dealList = new ArrayList<>();

    public Security getSecurityByCode(String code) {
        return securityIndex.get(code);
    }

    @Override
    public void insertClient(Client client) {
        clientList.add(client);
        clientIndex.put(client.getName(), client);
    }

    @Override
    public Client getClientByName(String name) {
        return clientIndex.get(name);
    }

    @Override
    public Set<Client> getAllClients() {
        return new LinkedHashSet<>(clientList);
    }

    @Override
    public void insertDeal(Deal deal) {
        dealList.add(deal);
    }

    @Override
    public List<Deal> getAllDeals() {
        return new ArrayList<>(dealList);
    }
}
