package com.zs.dao;

public class DaoFactory {

    private static Dao dao;

    public static Dao getInstance() {
        if (dao == null) {
            dao = new MemDao();
        }
        return dao;
    }
}
