package com.zs;

import com.zs.entity.Security;

import java.util.HashMap;
import java.util.Map;

/**
 * Класс для хранения очередей заявок и механизмов мэтчинга заявок.
 * Для каждой ценной бумаги формируется своя очередь заявок и механизм мэтчинга над этой очередью
 */
public class OrderQueueMatcherHolder {
    private static Map<Security, OrderQueue> queueMap = new HashMap<>();
    private static Map<Security, OrderMatcher> matcherMap = new HashMap<>();

    public static OrderQueue getOrderQueueFor(Security security) {
        ensureQueueExists(security);
        return queueMap.get(security);
    }

    private static void ensureQueueExists(Security security) {
        if (queueMap.get(security) == null) {
            OrderQueue q = new OrderQueue(security);
            OrderMatcher m = new OrderMatcher(q);
            queueMap.put(security, q);
            matcherMap.put(security, m);
        }
    }
}
