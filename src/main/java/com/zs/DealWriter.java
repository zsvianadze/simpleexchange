package com.zs;

import com.zs.dao.DaoFactory;
import com.zs.entity.Deal;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Класс для записи сформированных сделок в файл
 */
public class DealWriter {

    public static void write(String fileName) throws IOException {
        try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(fileName)))) {
            DaoFactory.getInstance().getAllDeals().stream().map(DealWriter::formatDeal).forEach(pw::println);
        }
    }

    private static String formatDeal(Deal deal) {
        StringBuilder dealStrBuilder = new StringBuilder();
        dealStrBuilder.append(deal.getBuyOrder().getClient().getName()).append("\t")
                .append(deal.getSellOrder().getClient().getName()).append("\t")
                .append(deal.getBuyOrder().getSecurity().getCode()).append("\t")
                .append(deal.getPrice()).append("\t").append(deal.getQuantity());
        return dealStrBuilder.toString();
    }
}
