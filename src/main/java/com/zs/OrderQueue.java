package com.zs;

import com.zs.entity.Order;
import com.zs.entity.Security;
import com.zs.ref.DealDirection;

import java.util.Comparator;
import java.util.Observable;
import java.util.PriorityQueue;

/**
 * Очередь заявок.
 * Представляет из себя две очереди: заявки на покупку и заявки на продажу.
 * Очереди отсортированы в соответствии с приоритетами: заявка на покупку с наибольшей ценой имеет наибольший приоритет
 * в очереди на покупку, заявка на продажу с наименьшей ценой имеет наивысший приоритет в очереди на продажу.
 */
public class OrderQueue extends Observable {

    private Security security;
    //Очередь заявок на покупку остортирована по убыванию цены покупки
    private PriorityQueue<Order> buyQueue = new PriorityQueue<>(Comparator.comparing(Order::getPrice).reversed());
    //Очередь заявок на продажу остортирована по возрастанию цены продажи
    private PriorityQueue<Order> sellQueue = new PriorityQueue<>(Comparator.comparing(Order::getPrice));

    public OrderQueue(Security security) {
        this.security = security;
    }

    public void add(Order order) {
        PriorityQueue<Order> queue = order.getDirection() == DealDirection.BUY ? buyQueue : sellQueue;
        queue.add(order);
        setChanged();
        notifyObservers();
    }

    public boolean isBuyEmpty() {
        return buyQueue.isEmpty();
    }

    public Order pollBuy() {
        return buyQueue.poll();
    }

    public Order peekBuy() {
        return buyQueue.peek();
    }

    public boolean isSellEmpty() {
        return sellQueue.isEmpty();
    }

    public Order pollSell() {
        return sellQueue.poll();
    }

    public Order peekSell() {
        return sellQueue.peek();
    }
}
