package com.zs;

import com.zs.entity.*;

import java.math.BigDecimal;

/**
 * Класс, выполняющий изменение балансов клиентов, заключивших сделку
 */
public class ClientBalanceUpdater {

    public static void updateWith(Deal deal) {
        changeClientBalance(deal.getBuyOrder().getClient(), deal);
        changeClientBalance(deal.getSellOrder().getClient(), deal);
    }

    public static void changeClientBalance(Client client, Deal deal) {
        BigDecimal dealQuantity = deal.getQuantity();
        BigDecimal dealAmount = dealQuantity.multiply(deal.getPrice());

        if (isSeller(client, deal)) {
            dealQuantity = dealQuantity.negate();
        } else {
            dealAmount = dealAmount.negate();
        }

        ClientBalance clientBalance = client.getClientBalance();

        MoneyAccount moneyAccount = clientBalance.getMoneyAccount();
        moneyAccount.setAmount(moneyAccount.getAmount().add(dealAmount));

        SecurityAccount securityAccount = clientBalance.getSecurityAccountFor(deal.getBuyOrder().getSecurity());
        if (securityAccount == null) {
            securityAccount = new SecurityAccount(deal.getBuyOrder().getSecurity(), BigDecimal.ZERO);
            clientBalance.addSecurityAccount(securityAccount);
        }
        securityAccount.setQuantity(securityAccount.getQuantity().add(dealQuantity));
    }

    private static boolean isSeller(Client client, Deal deal) {
        return deal.getSellOrder().getClient().equals(client);
    }
}
