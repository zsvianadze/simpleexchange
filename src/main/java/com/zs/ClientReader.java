package com.zs;

import com.zs.dao.Dao;
import com.zs.dao.DaoFactory;
import com.zs.entity.Client;
import com.zs.entity.ClientBalance;
import com.zs.entity.MoneyAccount;
import com.zs.entity.SecurityAccount;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Класс для считывания начальных балансов клиентов из файла
 */
public class ClientReader {

    public static void read(String fileName) throws IOException {
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.map(ClientReader::parseClient).forEach(c -> DaoFactory.getInstance().insertClient(c));
        }
    }

    private static Client parseClient(String clientStr) {
        Dao dao = DaoFactory.getInstance();
        String[] fields = clientStr.split("\t");
        String clientName = fields[0];
        MoneyAccount moneyAccount = new MoneyAccount(new BigDecimal(fields[1]));
        SecurityAccount securityAccountA = new SecurityAccount(dao.getSecurityByCode("A"), new BigDecimal(fields[2]));
        SecurityAccount securityAccountB = new SecurityAccount(dao.getSecurityByCode("B"), new BigDecimal(fields[3]));
        SecurityAccount securityAccountC = new SecurityAccount(dao.getSecurityByCode("C"), new BigDecimal(fields[4]));
        SecurityAccount securityAccountD = new SecurityAccount(dao.getSecurityByCode("D"), new BigDecimal(fields[5]));
        ClientBalance clientBalance = new ClientBalance(
                moneyAccount, Arrays.asList(securityAccountA, securityAccountB, securityAccountC, securityAccountD));
        return new Client(clientName, clientBalance);
    }
}
